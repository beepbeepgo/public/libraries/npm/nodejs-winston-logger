const { chai, sinon, sinonChai } = require("@beepbeepgo/nodejs-testing-common");

const { expect } = chai;
const winston = require("winston");
const DailyRotateFile = require("winston-daily-rotate-file");
const createLogger = require("../src/index");

chai.use(sinonChai);

// class ConsoleMock extends winston.transports.Console {
//   constructor(options) {
//     super(options);
//     this.logSpy = sinon.spy();
//   }

//   log(info, callback) {
//     this.logSpy(info);
//     super.log(info, callback);
//   }
// }

describe("createLogger", () => {
  afterEach(() => {
    sinon.restore();
  });

  it("should create a logger with default options", () => {
    const logger = createLogger("test-service");

    expect(logger).to.be.an("object");
    expect(logger).to.have.property("level", "info");
    expect(logger).to.have.property("transports");
    expect(logger.transports).to.have.lengthOf(2);
    expect(logger.transports[0]).to.be.an.instanceof(DailyRotateFile);
    expect(logger.transports[1]).to.be.an.instanceof(DailyRotateFile);
  });

  it("should create a logger with custom options", () => {
    const options = {
      logDir: "custom_logs",
      errorFilename: "custom-error-%DATE%.log",
      infoFilename: "custom-info-%DATE%.log",
      datePattern: "YYYY-MM",
      zippedArchive: false,
      maxSize: "10m",
      maxFiles: "7d",
    };

    const logger = createLogger("test-service", options);

    expect(logger).to.be.an("object");
    expect(logger).to.have.property("level", "info");
    expect(logger).to.have.property("transports");
    expect(logger.transports).to.have.lengthOf(2);
    expect(logger.transports[0]).to.be.an.instanceof(DailyRotateFile);
    expect(logger.transports[1]).to.be.an.instanceof(DailyRotateFile);
  });

  it("should add console transport in non-production and non-test environments", () => {
    const originalEnv = process.env.NODE_ENV;
    process.env.NODE_ENV = "development";

    const logger = createLogger("test-service");

    expect(logger).to.be.an("object");
    expect(logger).to.have.property("level", "info");
    expect(logger).to.have.property("transports");
    expect(logger.transports).to.have.lengthOf(3);
    expect(logger.transports[0]).to.be.an.instanceof(DailyRotateFile);
    expect(logger.transports[1]).to.be.an.instanceof(DailyRotateFile);

    process.env.NODE_ENV = originalEnv;
  });

  it("should not add console transport in production environment", () => {
    const originalEnv = process.env.NODE_ENV;
    process.env.NODE_ENV = "production";

    const logger = createLogger("test-service");

    expect(logger).to.be.an("object");
    expect(logger).to.have.property("level", "info");
    expect(logger).to.have.property("transports");
    expect(logger.transports).to.have.lengthOf(2);
    expect(logger.transports[0]).to.be.an.instanceof(DailyRotateFile);
    expect(logger.transports[1]).to.be.an.instanceof(DailyRotateFile);

    process.env.NODE_ENV = originalEnv;
  });
  it("should not add console transport in test environment", () => {
    const originalEnv = process.env.NODE_ENV;
    process.env.NODE_ENV = "test";

    const logger = createLogger("test-service");

    expect(logger).to.be.an("object");
    expect(logger).to.have.property("level", "info");
    expect(logger).to.have.property("transports");
    expect(logger.transports).to.have.lengthOf(2);
    expect(logger.transports[0]).to.be.an.instanceof(DailyRotateFile);
    expect(logger.transports[1]).to.be.an.instanceof(DailyRotateFile);

    process.env.NODE_ENV = originalEnv;
  });

  it("should log message with meta data in non-production and non-test environments", () => {
    const originalEnv = process.env.NODE_ENV;
    process.env.NODE_ENV = "development";

    const logger = createLogger("test-service");
    const consoleTransport = logger.transports.find(
      (t) => t instanceof winston.transports.Console
    );

    const consoleLogSpy = sinon.stub(consoleTransport, "log");
    const meta = { key: "value" };

    logger.info("test message", meta);

    expect(consoleLogSpy).to.have.been.calledOnce;
    expect(consoleLogSpy.firstCall.args[0].message).to.equal("test message");
    expect(consoleLogSpy.firstCall.args[0])
      .to.have.property("metadata")
      .to.contain(meta);

    consoleLogSpy.restore();
    process.env.NODE_ENV = originalEnv;
  });

  it("should log message without meta data in non-production and non-test environments", () => {
    const originalEnv = process.env.NODE_ENV;
    process.env.NODE_ENV = "development";

    const logger = createLogger("test-service");
    const consoleTransport = logger.transports.find(
      (t) => t instanceof winston.transports.Console
    );
    const logStub = sinon.stub(consoleTransport, "log");

    logger.info("test message");

    expect(logStub).to.have.been.calledOnce;
    expect(logStub.firstCall.args[0].message).not.to.contain("{}");

    process.env.NODE_ENV = originalEnv;
  });
});
