# Winston Logger

A simple and configurable logger utility for Node.js applications using the Winston library and the DailyRotateFile transport. This logger utility allows you to create loggers for different services and manage log files with automatic rotation based on date and size.

## Features

- Configurable logging directory, file naming, and date patterns
- Separate log files for error and info level logs
- Automatic log file rotation based on date and size
- Optional zipped archive for rotated log files
- Console logging for non-production and non-test environments with customizable output format

## Installation

First, you need to install the required dependencies:

```bash
npm i @beepbeepgo/winston-logger
```

## Usage

To use this logger utility, simply import the createLogger function and create a new logger by providing a service name.

```javascript
const createLogger = require('@beepbeepgo/winston-logger');
const logger = createLogger('my-service');
```

You can log messages using the info, warn, error, and other methods provided by the Winston library.

```javascript
logger.info('This is an info message');
logger.error('This is an error message');
```

## Configuration

You can customize the logger by passing an options object when creating a new logger.

```javascript
const options = {
  logDir: 'custom_logs', // Custom log directory
  errorFilename: 'custom-error-%DATE%.log', // Custom error log filename pattern
  infoFilename: 'custom-info-%DATE%.log', // Custom info log filename pattern
  datePattern: 'YYYY-MM-DD', // Custom date pattern
  zippedArchive: true, // Enable or disable zipped archive for rotated files
  maxSize: '10m', // Maximum log file size before rotation
  maxFiles: '7d', // Maximum number of log files to keep
};

const logger = createLogger('my-service', options);
```

## Environments

By default, console logging is enabled for non-production and non-test environments. If the NODE_ENV environment variable is set to production or test, console logging will be disabled.

```sh
export NODE_ENV=production
```
