const winston = require("winston");
const DailyRotateFile = require("winston-daily-rotate-file");

const createLogger = (service, options = {}) => {
  const {
    logDir = "logs",
    errorFilename = "error-%DATE%.log",
    infoFilename = "info-%DATE%.log",
    datePattern = "YYYY-MM-DD",
    zippedArchive = true,
    maxSize = "20m",
    maxFiles = "14d",
  } = options;

  const transports = [
    new DailyRotateFile({
      level: "error",
      filename: `${logDir}/${errorFilename}`,
      datePattern,
      zippedArchive,
      maxSize,
      maxFiles,
    }),
    new DailyRotateFile({
      level: "info",
      filename: `${logDir}/${infoFilename}`,
      datePattern,
      zippedArchive,
      maxSize,
      maxFiles,
    }),
  ];

  if (
    process.env.NODE_ENV !== "production" &&
    process.env.NODE_ENV !== "test"
  ) {
    transports.push(
      new winston.transports.Console({
        format: winston.format.combine(
          winston.format.colorize(),
          winston.format.timestamp(),
          winston.format.printf(({ timestamp, level, message, ...meta }) => {
            const metaData = Object.keys(meta).length
              ? JSON.stringify(meta)
              : "";
            return `[${timestamp}] [${level}] [${service}] ${message} ${metaData}`;
          }),
          winston.format.metadata()
        ),
      })
    );
  }

  const logger = winston.createLogger({
    level: "info",
    format: winston.format.combine(
      winston.format.timestamp(),
      winston.format.json()
    ),
    defaultMeta: { service: service },
    transports: transports,
  });

  return logger;
};

module.exports = createLogger;
